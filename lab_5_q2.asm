.model small
.stack 100h
.data
    
    v1 db "1"
    v2 db "?"
    v3 db 0ah,0dh, "gamez $"

.code
main proc
    
     mov ax, @data
     mov ds, ax
     
     
     mov dl, v1
     mov ah, 02
     int 21h
     
     mov bl, v2
     mov bl, 3
     mov dl, bl
     mov ah, 02
     int 21h
     
     
     mov dl, offset v3
     mov ah, 09
     int 21h
     
ret


