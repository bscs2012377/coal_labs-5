.model small
.stack 100h
.data

var1 db "ayush $"
var2 db "veerban $"

.code
main proc
    
    mov ax,@data
    mov ds,ax
    mov dl, offset var1
    mov ah, 09
    int 21h
  
    mov bl, offset var2
    mov dl,bl
    mov ah, 09
    int 21h
    
    
main endp
end main
