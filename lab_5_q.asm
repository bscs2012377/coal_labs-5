
.model small
.stack 100h
.data
     var1 db  "A.V $"
     var2 db 0Ah,0Dh,"A.V" 
.code
main proc
        
        mov ax,@data
        mov ds,ax
        mov dl,offset var1
        mov ah,09
        int 21h
        mov ah,4ch
        mov bl, offset var2
        mov dl,bl
        mov ah,09
        int 21h  
        mov dl,36
        mov ah,02
        int 21h
      
        
        
    main endp
end main